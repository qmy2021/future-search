import { createApp } from 'vue'
import ElementPlus from 'element-plus';
import 'element-plus/theme-chalk/index.css'
import App from './App.vue'
import router from './router'
import vuex from './store'
const app = createApp(App)
app.use(ElementPlus)
app.use(router)
app.use(vuex)
app.mount('#app') 