import { createRouter, createWebHistory } from 'vue-router'
import Index from '../views/Index.vue'
import Home from '../components/Home.vue'
const routes = [
  { path: '/', redirect: '/Home' },
  {
    path: '/Index',
    name: 'Index',
    component:Index
  },
  {
    path: '/Home',
    name: 'Home',
    component:Home
  },
]
const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes 
})
export default router 
